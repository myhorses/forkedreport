## Forked Report

Forked Report is a from scratch reimplementation of Fortes Report.  

##### Objectives:

- Update UI.
- Cleaning code.
- Maintain the compatibility of names of components, functions and properties.

---

#### Original project:

You can get the original version of Fortes Report CE at:  
https://github.com/fortesinformatica/fortesreport-ce  

##### Collaborated to the origin project:

- Ronaldo Moreira
- Márcio Martins
- Régys Borges da Silveira
- Juliomar Marchetti
- Luis Michel Silva Moreira
- Silvio Clecio
- Isaac Trindade
- Luiz Américo
- Daniel Simões de Almeida
- And more others...

---

Copyright (c) 1999-2021 Fortes Tecnologia and Collaborators  
Copyright (c) 2022-2024 Raymond Risko, RiskoDEV and Collaborators  
